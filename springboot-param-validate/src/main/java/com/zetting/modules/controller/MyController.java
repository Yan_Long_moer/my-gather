package com.zetting.modules.controller;

import com.zetting.modules.dto.ValidateRequest;
import com.zetting.base.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 切面实现入参校验
 */
@RestController
public class MyController {

    @GetMapping(value = "/validate")
    public Response validate(ValidateRequest request) {
        return Response.success();
    }
}
