package com.zetting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author zetting
 * @date 2018-12-21 22:30
 */
@SpringBootApplication
public class LogInterceptorsLogtagApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogInterceptorsLogtagApplication.class, args);
    }
}
